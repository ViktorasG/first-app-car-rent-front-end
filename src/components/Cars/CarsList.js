import React, { Component } from 'react';

import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
} from 'reactstrap';

import './CarsList.css';

class CarsList extends Component {
  render() {
    return (
      <div className="row">
        {this.props.items.map((item, index) => (
          <div key={index} className="col-3">
            <Card>
              <CardImg
                top
                width="100%"
                src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
                alt="Card image cap"
              />
              <CardBody>
                <CardTitle>{item.pavadinimas}</CardTitle>
                <CardSubtitle>{item.kategorija}</CardSubtitle>
                <CardText>
                  Some info about this book!!! {item.autorius}
                </CardText>
                <Button onClick={() => this.props.deleteCar(item.id)}>Delete</Button>
              </CardBody>
            </Card>
          </div>
        ))}
      </div>
    );
  }
}

export default CarsList;
