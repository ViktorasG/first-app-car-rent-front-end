import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import './App.css';

import NavBar from './components/NavBar';
import Cars from './containers/Cars';
import RentalLocations from './containers/Locations';
// import TodoList from './containers/TodoList';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <NavBar />

          <div className="container">
            <Route exact path="/" component={Cars} />
            <Route path="/cars" component={Cars} />
            <Route path="/locations" component={RentalLocations} />
          </div>
        </div>
      </Router>    
    );
  }
}

export default App;
