import React, { Component } from 'react';

import { Button, ListGroup, ListGroupItem } from 'reactstrap';

import HelloMessage from '../../components/HelloMessage';

class RentalLocations extends Component {
  constructor(props) {
    super(props);

    this.tick = this.tick.bind(this);

    this.state = {
      seconds: 0,
    };
  }

  tick() {
    this.setState(state => ({
      seconds: state.seconds + 1,
    }));
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <div>
        <HelloMessage name="RentalLocations" />

        <div className="d-flex flex-column">
          Seconds: {this.state.seconds}
          <Button color="primary" onClick={this.tick}>
            Increase seconds!
          </Button>
        </div>
      </div>
    );
  }
}

export default RentalLocations;