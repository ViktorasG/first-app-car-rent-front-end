import React, { Component } from 'react';
import axios from 'axios';

import { Button, Form, FormGroup, Label, Input, Alert, Col } from 'reactstrap';

import HelloMessage from '../../components/HelloMessage';
import BooksList from '../../components/Books/BooksList';

class Cars extends Component {
  constructor(props) {
    super(props);

    this.deleteCar = this.deleteCar.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleAuthorChange = this.handleAuthorChange.bind(this);
    this.handleCategoryChange = this.handleCategoryChange.bind(this);
    this.handleYearChange = this.handleYearChange.bind(this);

    this.state = {
      books: [],
      title: '',
      author: '',
      year: '',
      category: '',
      error: undefined,
    };
  }

  componentDidMount() {
    this.fetchCars();
  }

  fetchCars() {
    axios
      .get('https://test-bta-books-api.herokuapp.com/books')
      .then(cars => {
        this.setState(state => ({
          cars: cars.data,
          error: undefined,
        }));
      })
      .catch(error => {
        this.setState(state => ({
          cars: state.cars,
          error: error.message,
        }));
      });
  }

  addCar(car) {
    axios
      .post('https://test-bta-books-api.herokuapp.com/books', car)
      .then(() => {
        console.log(car);
        this.setState(state => ({
          cars: state.cars.concat(this.mapToCar(car)),
          error: undefined,
        }));

        this.clearForm();
      })
      .catch(error => {
        this.setState(state => ({
          cars: state.cars,
          error: error.message,
        }));
      });
  }

  deleteCar(carId) {
    axios
      .delete(`https://test-bta-books-api.herokuapp.com/books/${carId}`)
      .then(cars => {
        this.setState(state => ({
          cars: state.cars.filter(car => car.id !== carId),
          error: undefined,
        }));
      })
      .catch(error => {
        this.setState(state => ({
          cars: state.cars,
          error: error.message,
        }));
      });
  }

  clearForm() {
    this.setState({
      title: '',
      author: '',
      year: '',
      category: '',
    });
  }

  handleTitleChange(e) {
    this.setState({ title: e.target.value });
  }

  handleAuthorChange(e) {
    this.setState({ author: e.target.value });
  }

  handleYearChange(e) {
    this.setState({ year: e.target.value });
  }

  handleCategoryChange(e) {
    this.setState({ category: e.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();

    if (!this.state.title.length) {
      return;
    }
    if (!this.state.category.length) {
      return;
    }
    if (!this.state.author.length) {
      return;
    }
    if (!this.state.year.length) {
      return;
    }

    const newCar = {
      title: this.state.title,
      author: this.state.author,
      year: this.state.year,
      category: this.state.category,
      bestSeller: true,
    };

    this.addCar(newCar);
  }

  mapToCar(car) {
    return {
      id: car.id,
      pavadinimas: car.title,
      autorius: car.author,
      kategorija: car.category,
    }
  }

  render() {
    return (
      <div>
        <HelloMessage name="Cars" />

        <hr />

        <Form onSubmit={this.handleSubmit}>
          <FormGroup row>
            <Label for="title" sm={2}>
              Title
            </Label>
            <Col sm={10}>
              <Input
                type="title"
                name="title"
                id="title"
                placeholder="Enter car title"
                onChange={this.handleTitleChange}
                value={this.state.title}
              />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label for="author" sm={2}>
              Author
            </Label>
            <Col sm={10}>
              <Input
                type="author"
                name="author"
                id="author"
                placeholder="Enter car author"
                onChange={this.handleAuthorChange}
                value={this.state.author}
              />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label for="category" sm={2}>
              Category
            </Label>
            <Col sm={4}>
              <Input
                type="category"
                name="category"
                id="category"
                placeholder="Enter car category"
                onChange={this.handleCategoryChange}
                value={this.state.category}
              />
            </Col>

            <Label for="year" sm={2}>
              Year
            </Label>
            <Col sm={4}>
              <Input
                type="year"
                name="year"
                id="year"
                placeholder="Enter car release year"
                onChange={this.handleYearChange}
                value={this.state.year}
              />
            </Col>
          </FormGroup>

          <Button color="primary" size="lg" block>
            Add new book!
          </Button>
        </Form>

        <hr />

        {this.state.error ? (
          <Alert color="danger">{this.state.error}</Alert>
        ) : null}

        <CarsList items={this.state.cars} deleteCar={this.deleteCar} />
      </div>
    );
  }
}

export default Cars;
